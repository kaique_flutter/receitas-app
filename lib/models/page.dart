import 'package:flutter/cupertino.dart';

class Page {
  final String title;
  final Widget widget;

  Page({this.title, this.widget});
}
