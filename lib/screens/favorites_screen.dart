import 'package:daily_meals/widgets/meal_item.dart';
import 'package:flutter/material.dart';

import '../models/meal.dart';

class FavoritesScreen extends StatelessWidget {
  final List<Meal> favoritedMeals;

  const FavoritesScreen(this.favoritedMeals);

  @override
  Widget build(BuildContext context) {
    if (favoritedMeals.isEmpty) {
      return Center(
        child: Text('You have no favorites yet - start adding some'),
      );
    } else {
      return ListView.builder(
        itemBuilder: (ctx, i) {
          return MealItem(
            id: favoritedMeals[i].id,
            title: favoritedMeals[i].title,
            imageUrl: favoritedMeals[i].imageUrl,
            affordability: favoritedMeals[i].affordability,
            complexity: favoritedMeals[i].complexity,
            duration: favoritedMeals[i].duration
          );
        },
        itemCount: favoritedMeals.length,
      );
    }
  }
}
